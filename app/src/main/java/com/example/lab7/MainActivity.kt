package com.example.lab7

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btnIspis = findViewById(R.id.btnIspis) as Button
        var checkBox = findViewById(R.id.checkBox) as CheckBox
        var radioButton = findViewById(R.id.radioButton) as RadioButton
        var toggleButton = findViewById(R.id.toggleButton) as ToggleButton
        //var input = findViewById(R.id.input) as TextView
        val input = findViewById(R.id.input) as TextView

        btnIspis.setOnClickListener(){
            Toast.makeText(this, input.getText().toString() + checkBox.getText().toString() + radioButton.getText().toString() + toggleButton.getText().toString(), Toast.LENGTH_LONG).show()
        }

        checkBox.setOnClickListener(){
            if (checkBox.isChecked){
                Toast.makeText(this, "CheckBox oznacen", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(this, "CheckBox nije oznacen", Toast.LENGTH_SHORT).show()
            }
        }

        radioButton.setOnClickListener(){
            if (radioButton.isActivated){
                Toast.makeText(this, "RadioButton oznacen", Toast.LENGTH_SHORT).show()
            }
        }

        toggleButton.setOnClickListener(){
            if (toggleButton.isChecked){
                Toast.makeText(this, "ToggleButton ukljucen", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(this, "ToggleButton iskljucen", Toast.LENGTH_SHORT).show()
            }
        }

    }
}

